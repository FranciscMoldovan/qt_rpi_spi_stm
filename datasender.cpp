#include "datasender.h"

DataSender::DataSender()
{
    data = new std::vector<unsigned char>();
}

DataSender::~DataSender()
{
    delete data;
}


void DataSender::SetData(std::vector<unsigned char>& dataSource)
{
    // 1 empty the current vector of values
    this->data->clear();
    for (unsigned char c: dataSource)
    {
        this->data->push_back(c);
    }
}

std::vector<unsigned char>* DataSender::GetData()
{
    return this->data;
}
