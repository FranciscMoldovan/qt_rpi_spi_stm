#ifndef SPICOMM_H
#define SPICOMM_H
#include <stdint-gcc.h>
#include <QObject>

#define ARRAY_SIZE(a) sizeof(a)/sizeof(a[0])

class SPIComm : public QObject
{
    Q_OBJECT
public:
    SPIComm();
    int configure(const char *device);
    void setPacketSize(uint8_t size);
    void transfer(int fd);
    void changeTX(uint8_t *newData);

private:


    const char *device = "/dev/spidev0.0";
    uint8_t mode;
    uint8_t bits;
    uint32_t speed;
    uint32_t ddelay;
    uint8_t *tx;
    uint8_t *rx;
    uint8_t packet_size;



};

#endif // SPICOMM_H
