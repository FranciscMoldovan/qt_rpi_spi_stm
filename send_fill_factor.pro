#-------------------------------------------------
#
# Project created by QtCreator 2016-10-25T16:51:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11
TARGET = send_fill_factor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        spicomm.cpp

HEADERS  += mainwindow.h\
            spicomm.h

FORMS    += mainwindow.ui
