#include "spicomm.h"
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <string.h>

SPIComm::SPIComm()
{
    packet_size = 4;
    tx = (uint8_t*) malloc(packet_size*sizeof(uint8_t));
    rx = (uint8_t*) malloc(packet_size*sizeof(uint8_t));
    *tx=0xDD; *(tx+1)=0xEE; *(tx+2)=0xAA; *(tx+3)=0xDD;
    this->bits = 8;
    this->speed = 500000;
    this->ddelay = 0xFFFF;
}

void SPIComm::transfer(int fd)
{
//    memset(rx, 0, packet_size);
//    memset(tx, 0, packet_size);

    int ret;
    struct spi_ioc_transfer tr;
    memset(&tr, 0, sizeof(tr));


    tr.tx_buf = (unsigned long)tx;
    tr.rx_buf = (unsigned long)rx;
    tr.len = this->packet_size;
    tr.delay_usecs = this->ddelay;
    tr.speed_hz = this->speed;
    tr.bits_per_word = this->bits;

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    if (ret <1)
    {
        perror("Can't send spi message");
    }

    for(ret=0; ret < this->packet_size; ret++)
    {
        if(!(ret%6))
            puts("");
        printf("%.2X ", rx[ret]);
    }
    puts("");

}


int SPIComm::configure(const char *device)
{
    int ret = 0;
    int fd;

    fd = open(device, O_RDWR);

    if (fd < 0)
    {
        perror("Can't open device");
    }

    // spi mode
    ret = ioctl(fd, SPI_IOC_RD_MODE, &this->mode);
    if(ret == -1)
    {
        perror("Can't get spi mode");
    }

    // bits per word
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &this->bits);
    if(ret == -1)
    {
        perror("can't get bits per word");
    }

    printf("spi mode: %d\n",this->mode);
    printf("bits per word: %d \n", this->bits);
    printf("max speed %d Hz (%d KHz)\n", this->speed, this->speed/1000);

    return fd;
}

void SPIComm::changeTX(uint8_t *newData)
{
    memcpy(tx, newData, packet_size);
}

void SPIComm::setPacketSize(uint8_t size)
{
    this->packet_size = size;
}






















