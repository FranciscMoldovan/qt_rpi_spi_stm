#ifndef DATASENDER_H
#define DATASENDER_H
#include <vector>

class DataSender
{
public:
    DataSender();
    ~DataSender();
    void SetData(std::vector<unsigned char>& dataSource);
    std::vector<unsigned char> *GetData();
private:
     std::vector<unsigned char>* data;

};

#endif // DATASENDER_H
