#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <spicomm.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    /////
    ui->slider_pwm1->setRange(0,255);
    ui->slider_pwm2->setRange(0,255);
    ui->slider_pwm3->setRange(0,255);
    ui->slider_pwm4->setRange(0,255);
    /////

    //////////////////////////////////////////////////////////////////////
    QObject::connect(ui->slider_pwm1, SIGNAL(valueChanged(int)),
                     ui->lcd_pwm1, SLOT(display(int)));

    QObject::connect(ui->slider_pwm2, SIGNAL(valueChanged(int)),
                     ui->lcd_pwm2, SLOT(display(int)));

    QObject::connect(ui->slider_pwm3, SIGNAL(valueChanged(int)),
                     ui->lcd_pwm3, SLOT(display(int)));

    QObject::connect(ui->slider_pwm4, SIGNAL(valueChanged(int)),
                     ui->lcd_pwm4, SLOT(display(int)));

    //QObject::connect(ui->btnSend, SIGNAL(released()), this, SLOT(handleButton()));

    QObject::connect(ui->slider_pwm1, SIGNAL(valueChanged(int)),
                     this, SLOT(handleButton()));

    QObject::connect(ui->slider_pwm2, SIGNAL(valueChanged(int)),
                     this, SLOT(handleButton()));

    QObject::connect(ui->slider_pwm3, SIGNAL(valueChanged(int)),
                    this, SLOT(handleButton()));

    QObject::connect(ui->slider_pwm4, SIGNAL(valueChanged(int)),
                     this, SLOT(handleButton()));

    //////////////////////////////////////////////////////////////////////
}

void MainWindow::handleButton()
{
    SPIComm mySPI;
    int fd = mySPI.configure("/dev/spidev0.0");
    mySPI.setPacketSize(4);

    uint8_t *val_pwm = (uint8_t*)malloc(4*sizeof(uint8_t));

        val_pwm[0] = (uint8_t) ui->slider_pwm1->value();
        val_pwm[1] = (uint8_t) ui->slider_pwm2->value();
        val_pwm[2] = (uint8_t) ui->slider_pwm3->value();
        val_pwm[3] = (uint8_t) ui->slider_pwm4->value();

        mySPI.changeTX(val_pwm);
        mySPI.transfer(fd);
}

Ui::MainWindow* MainWindow::getUi(){
    return ui;
}

void MainWindow::sendDataOverSPI()
{

}

MainWindow::~MainWindow()
{
    delete ui;
}
